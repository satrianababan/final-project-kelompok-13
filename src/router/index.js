import Vue from 'vue'
import VueRouter from 'vue-router'
import LupaPassword from '../views/LupaPassword'
import ResetPassword from '../views/ResetPassword'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: () => import(/* webpackChunkName: "about" */ '../views/HomeView.vue')
  },
  {
    path: '/blogs',
    name: 'Blogs',
    
    component: () => import(/* webpackChunkName: "about" */ '../views/BlogsView.vue')
  },
  {
    path: '/blog/:id',
    name: 'Blog',
    
    component: () => import(/* webpackChunkName: "about" */ '../views/BlogView.vue')
  },
  {
    path: '/kelolablogs',
    name: 'Kelolablogs',
    
    component: () => import(/* webpackChunkName: "about" */ '../views/KelolaBlogsView.vue')
  },
  {
    path: '/lupa-password',
    name: 'LupaPassword',
    component: LupaPassword
  },
  {
    path: '/reset-password',
    name: 'ResetPassword',
    component: ResetPassword
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})


export default router
